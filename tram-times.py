import gi, time, requests, json
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import Pango

class TimeLabel(Gtk.Window):
    
    KEY = '08dc5e36738b44f9abe2f42c1d6017c0'
    ID = 331 #Droylsden stop
    headers = {
        # Request headers
        'Ocp-Apim-Subscription-Key': KEY,
    }
    
    def __init__(self):
        Gtk.Window.__init__(self, title='Droylsden Tram')
        Gtk.Window.set_size_request(self, 480,320)
        Gtk.Window.set_decorated(self, False)
        self.box = Gtk.Box(spacing=1)
        self.add(self.box)
        
        self.textlabel = Gtk.Label()
        self.timelabel = Gtk.Label()
        self.textlabel.set_markup('<big>Next tram in:</big>')
        self.timelabel.set_markup('<big>Fetching</big>')
        self.box.pack_start(self.textlabel, True, True, 0)
        self.box.pack_start(self.timelabel, True, True, 0)
        
        GLib.timeout_add_seconds(15, self.get_wait_time)
        self.get_wait_time()
        
    def get_wait_time(self):
        try:
            r = requests.get('https://api.tfgm.com/odata/Metrolinks({0})'.format(self.ID), headers=self.headers)
            self.timelabel.set_text(str(r.json()['Wait0']))
            return GLib.SOURCE_CONTINUE
        except Exception as e:
            print("Request faild")
            return GLib.SOURCE_CONTINUE

window = TimeLabel()
window.connect("destroy", Gtk.main_quit)
window.show_all()
Gtk.main()
